﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum SandwichQuality
{
    fail,
    pass,
    perfect
}

public class Recipe {

    
    List<IngredientType> ingredients = new List<IngredientType>();

    int[] recipeIngredientLedger;

    public Recipe(List<IngredientType> ingreds)
    {
        ingredients = ingreds;
        recipeIngredientLedger = new int[System.Enum.GetNames(typeof(IngredientType)).Length];
        Tally(recipeIngredientLedger,ingredients);
    }

    void Tally(int[] ledger, List<IngredientType> list) //Populates ledger to reflect the number of each ingredientType in the list
    {
        for (int i = 0; i < ledger.Length; i++) //Reset list in case you have tallied it before
            ledger[i] = 0;
 
        foreach (IngredientType i in list)
            ledger[ (int)i ] ++;    //Add one to the ledger index pertaining to that particular ingredientType (enum -> int value)
    }

    public SandwichQuality Check(Sandwich sandwich)
    {
        Tally(recipeIngredientLedger, ingredients);  //probably unneccesary unless the recipe changes over time

        int[] sandwichIngredientLedger = new int[recipeIngredientLedger.Length];

        Tally(sandwichIngredientLedger, sandwich.ingredients);
        Debug.Log("Sandwich Ledger: " + ItemsInArray(sandwichIngredientLedger));
        Debug.Log("Recipe Ledger: " + ItemsInArray(recipeIngredientLedger));
        Debug.Log("Sandwich Elements: " + ItemsInList(sandwich.ingredients));
        Debug.Log("Recipe Elements: " + ItemsInList(ingredients));

        if (!recipeIngredientLedger.SequenceEqual(sandwichIngredientLedger))
            return SandwichQuality.fail;

        if (!ingredients.SequenceEqual(sandwich.ingredients))
            return SandwichQuality.pass;

        return SandwichQuality.perfect;
    }

    string ItemsInList<T>(List<T> list) {
        string s = null;
        for (int i = 0; i<list.Count; i++)
        {
            s += list[i].ToString();
            if(i!=list.Count - 1)
            {
                s += ", ";
            }
        }
        return s;
    }
    string ItemsInArray<T>(T[] array)
    {
        string s = null;
        for (int i = 0; i < array.Length; i++)
        {
            s += array[i].ToString();
            if (i != array.Length - 1)
            {
                s += ", ";
            }
        }
        return s;
    }
}
