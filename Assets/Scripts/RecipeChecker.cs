﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecipeChecker : MonoBehaviour {
    public TextMesh t;
    Recipe recipe;
    public List<IngredientType> ingredients;

	// Use this for initialization
	void Start () {
        recipe = new Recipe(ingredients);
        t.text = "";
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider other)
    {
        SandwichQuality q;
        Sandwich s = other.transform.root.gameObject.GetComponentInChildren<Sandwich>();

        if (s != null)
        {
            q = recipe.Check(s);
        }
        else return;

        switch (q)
        {
            case SandwichQuality.fail:
                t.text = "Fail";
                break;

            case SandwichQuality.pass:
                t.text = "Pass";
                break;

            case SandwichQuality.perfect:
                t.text = "Perfect";
                break;

        }

        StartCoroutine(ClearText(5f));
        Destroy(s.gameObject);

    }

    private IEnumerator ClearText(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        t.text = "";
        yield return null;
    }
}
