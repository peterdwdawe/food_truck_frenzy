﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Griddle : MonoBehaviour {

	public Vector3[] fryPositions;
    public bool[] fryPositionAvailable;
	public Vector3 stackPosition;

    public Collider fryingCollider;
    public Collider stackingCollider;

    public List<GameObject> stackedIngredients = new List<GameObject>();

	// Use this for initialization
	void Start () {
        fryPositionAvailable = new bool[fryPositions.Length];
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision collision)
    {
        foreach(ContactPoint p in collision.contacts)
        {
            if(p.thisCollider == fryingCollider)
            {
                AddToGrill(collision.gameObject);
                break;
            }
            if (p.thisCollider == stackingCollider)
            {
                AddToStack(collision.gameObject);
                break;
            }
        }
    }

    private void AddToGrill(GameObject g)
    {
        Ingredient ing = g.GetComponent<Ingredient>();
        if (ing != null)
        {
            if (!ing.inSandwich)
            {
                for (int i = 0; i < fryPositions.Length; i++)
                {

                    if (fryPositionAvailable[i])
                    {
                        ing.Fry(this,i);
                        break;
                    }
                }
            }
            else return;
        }
        else return;
    }

    private void Fry(Ingredient ingredient, int positionNumber)
    {
        throw new NotImplementedException();
    }

    private void AddToStack(GameObject gameObject)
    {
        throw new NotImplementedException();
    }

    
}
