﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void ControllerEvent(handSide side);
[System.Serializable]
public enum handSide
{
    right,
    left
}

public class Hand : MonoBehaviour
{
    //[HideInInspector]
    public GrabbableObject currentHeldObject;
    [SerializeField]
    GrabbableObject grabTarget;

    //public static event ControllerEvent PrimaryButtonPushed;
    //public static event ControllerEvent SecondaryButtonPushed;
    //public static event ControllerEvent TriggerPulled;
    //public static event ControllerEvent GripPulled;
    //public static event ControllerEvent PrimaryButtonReleased;
    //public static event ControllerEvent SecondaryButtonReleased;
    //public static event ControllerEvent TriggerReleased;
    //public static event ControllerEvent GripReleased;

    [HideInInspector]
    public OculusInput.ControllerInput controller;

    public handSide hand;
    SphereCollider interactionTrigger;
    public float interactionRadius;
    // Use this for initialization
    void Start()
    {
        if (hand == handSide.right)
            controller = OculusInput.instance.rightHand;
        else controller = OculusInput.instance.leftHand;

        interactionTrigger = gameObject.AddComponent<SphereCollider>();
        interactionTrigger.radius = interactionRadius;
        interactionTrigger.isTrigger = true;

    }

    // Update is called once per frame
    void Update()
    {
        Collider[] collidersInRange = Physics.OverlapSphere(transform.position,interactionRadius);
        float minDist = Mathf.Infinity;
        GrabbableObject minObj = null;
        if (currentHeldObject == null)
        {
            foreach (Collider c in collidersInRange)
            {
                GrabbableObject g = c.gameObject.GetComponentInParent<GrabbableObject>();
                if (g != null)
                {
                    if (!g.inHand)
                    {
                        float dist = Mathf.Abs((c.transform.position - transform.position).sqrMagnitude);
                        if (dist < minDist)
                        {
                            minDist = dist;
                            minObj = g;
                        }
                    }
                }
            }
            HighlightThisFrame(minObj);
        }
        if (OVRInput.GetUp(controller.grip))
        {
            if (currentHeldObject != null)
            {
                currentHeldObject.LetGo(this);
            }
            //currentHeldObject = null;
        }

        if (OVRInput.GetDown(controller.grip))
        {
            if (currentHeldObject != null)
                currentHeldObject.LetGo(this);

            if (grabTarget != null)
            {
                grabTarget.Grab(this);
                grabTarget = null;
                //grabTarget.UnHighlight(this);
            }

            //currentHeldObject = grabTarget;
        }


    }

    void HighlightThisFrame(GrabbableObject targetNew)
    {
        if (targetNew != grabTarget)
        {
            if (grabTarget != null)
                grabTarget.UnHighlight(this);
            if (targetNew != null)
                targetNew.Highlight(this);
            grabTarget = targetNew;
        }
    }
}
