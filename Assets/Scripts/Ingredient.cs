﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum IngredientType
{
    BottomBun,
    TopBun,

    BeefPatty,
    ChickenPatty,
    FishPatty,
    VeggiePatty,
    Egg,
    Bacon,

    Cheese,
    Tomato,
    Lettuce,
    Onion,
    Pickle,

    Ketchup,
    Mustard,
    Relish,
    Mayo,
    HotSauce,
    SecretSauce
}
public enum FryQuality
{
    Uncooked,
    Undercooked,
    Perfect,
    Overcooked,
    Burnt
}

public class Ingredient : PickupObject
{
    float fryAmount = 0;
    bool debugMode = false;
    GameObject test;

    public IngredientType ingredientType;
    [HideInInspector] public bool inSandwich = false;
    [HideInInspector] public Sandwich parentSandwich = null;

    [Tooltip("Bottom point of mesh in local coordinates")]
    public Vector3 bottomPosition = Vector3.zero;
    [Tooltip("Top point of mesh in local coordinates")]
    public Vector3 topPosition = Vector3.zero;
    [Tooltip("Probably needs to be zero to not fuck shit up. maybe needs to be fixed?")]
    public Vector3 placementEulerangles = Vector3.zero;

    [Tooltip("Time in seconds to fry completely")]
    public float fryTime = 5f;

    [Tooltip("Debug Mode Only. Ingredient prefab which should be used for testing placement.")]
    public GameObject addIngredientTestObject;



    // Use this for initialization
    protected override void Start () {
        base.Start();
    }

    // Update is called once per frame
    protected override void Update () {
        if (inSandwich)
        {
            inHand = parentSandwich.inHand;

            if (GetComponent<Renderer>() != null)
                HandleHighlight(parentSandwich.LeftHighlight, parentSandwich.RightHighlight);
        }
        else if (GetComponent<Renderer>() != null)
        {
            HandleHighlight(LeftHighlight, RightHighlight);
        }
        if (debugMode)
        {
            if (test != null)
            {
                test.transform.localPosition = topPosition - 
                    transform.InverseTransformVector(test.transform.TransformVector(test.GetComponent<Ingredient>().bottomPosition));

                test.transform.localEulerAngles = test.GetComponent<Ingredient>().placementEulerangles;
            }
        }
    }
    public void Fry(Griddle griddle, int fryPosition)
    {
        throw new NotImplementedException();
    }

    public FryQuality GetFryQuality()
    {
        if (fryAmount < 0.75f)
            return FryQuality.Uncooked;
        if (fryAmount < 0.9f)
            return FryQuality.Undercooked;
        if (fryAmount <= 1.1f)
            return FryQuality.Perfect;
        if (fryAmount <= 1.25f)
            return FryQuality.Overcooked;
        return FryQuality.Burnt;
    }

    public override void Grab(Hand hand)
    {
        if (inSandwich)
        {
            if (parentSandwich != null)
                parentSandwich.Grab(hand);
            else
                inSandwich = false;
        }
        if(!inSandwich)
            base.Grab(hand);
    }

    public override void LetGo(Hand hand)
    {
        if (inSandwich)
        {
            if (parentSandwich != null)
                parentSandwich.LetGo(hand);
            else
                inSandwich = false;
        }
        if (!inSandwich)
            base.LetGo(hand);
    }
    public override void Highlight(Hand hand)
    {
        if (inSandwich)
        {
            if (parentSandwich != null)
                parentSandwich.Highlight(hand);
            else
                inSandwich = false;
        }
        if (!inSandwich)
            base.Highlight(hand);
    }

    public override void UnHighlight(Hand hand)
    {
        if (inSandwich)
        {
            if (parentSandwich != null)
                parentSandwich.UnHighlight(hand);
            else
                inSandwich = false;
        }
        if (!inSandwich)
            base.UnHighlight(hand);
    }

    public void ToggleDebug()
    {

        if (debugMode)
        {
            if (test != null){
                Destroy(test);
            }
        }
        else
        {
            test = Instantiate(addIngredientTestObject, transform);
            test.transform.localScale = new Vector3(test.transform.localScale.x / transform.lossyScale.x, test.transform.localScale.y / transform.lossyScale.y, test.transform.localScale.z / transform.lossyScale.z);
        }

        debugMode = !debugMode;
    }

    //public void TestAddIngredient()
    //{
    //    StartCoroutine(ShowSphere());
    //}

    //IEnumerator ShowSphere()
    //{
    //    GameObject g = Instantiate(addIngredientTestObject,transform);
    //    g.transform.localPosition = topPosition - addIngredientTestObject.GetComponent<Ingredient>().bottomPosition;
    //    g.transform.localEulerAngles = addIngredientTestObject.GetComponent<Ingredient>().placementEulerangles;
    //    g.transform.localScale = new Vector3(g.transform.localScale.x / transform.lossyScale.x, g.transform.localScale.y / transform.lossyScale.y, g.transform.localScale.z / transform.lossyScale.z);
    //    yield return new WaitForSecondsRealtime(6f);
    //    Destroy(g);
    //}
}
