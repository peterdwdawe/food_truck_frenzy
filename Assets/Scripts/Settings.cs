﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class Settings : MonoBehaviour {

    public static Settings instance;

    public Color rightHighlight = Color.red;
    public Color leftHighlight = Color.blue;
    public Color bothHighlight = Color.yellow;

    private void Awake()
    {
        if (instance != null)
            Destroy(this);
        instance = this;
    }
}
