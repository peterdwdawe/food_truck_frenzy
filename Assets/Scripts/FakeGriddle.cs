﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeGriddle : MonoBehaviour {

    public GameObject PE;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator destroy1sec(GameObject g)
    {
        yield return new WaitForSeconds(1f);
        Destroy(g);
    }

    private void OnCollisionEnter(Collision collision)
    {
        GameObject effect = Instantiate(PE);
        PE.transform.position = collision.contacts[0].point;
        StartCoroutine(destroy1sec(effect));
    }
}
