﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupObject : GrabbableObject
{
    Vector3 lastEulerAngles = Vector3.zero;
    Vector3 lastPosition = Vector3.zero;
    [HideInInspector] public Rigidbody rb;
    
    void OnEnable()
    {
        if (GetComponent<Rigidbody>() == null)
        {
            gameObject.AddComponent<Rigidbody>();
        }
        rb = GetComponent<Rigidbody>();
        rb.isKinematic = true;
    }
    
    // Use this for initialization
    protected override void Start()
    {
        base.Start();

    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        
    }

    public override void Grab(Hand hand)
    {
        base.Grab(hand);
        rb.isKinematic = true;
        transform.parent = hand.transform;
        transform.localPosition = Vector3.zero;
    }


    public override void LetGo(Hand hand)
    {
        base.LetGo(hand);
        rb.isKinematic = false;
        transform.parent = null;
        rb.velocity = (transform.position - lastPosition) / Time.deltaTime;
        rb.angularVelocity = (transform.eulerAngles - lastEulerAngles) / Time.deltaTime;
    }

    private void LateUpdate()
    {
        lastPosition = transform.position;
        lastEulerAngles = transform.eulerAngles;
    }
}
