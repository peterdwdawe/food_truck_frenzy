﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class OculusInput : MonoBehaviour
{

    public struct ControllerInput
    {
        public OVRInput.Button button1; // A/X button
        public OVRInput.Button button2; // B/Y button
        public OVRInput.Button trigger;
        public OVRInput.Button grip;
        public ControllerInput(OVRInput.Button one, OVRInput.Button two, OVRInput.Button Trigger, OVRInput.Button Grip)
        {
            button1 = one;
            button2 = two;
            trigger = Trigger;
            grip = Grip;
        }
    }

    public ControllerInput rightHand;
    public ControllerInput leftHand;
    public static OculusInput instance;

    private void Start()
    {
        rightHand = new ControllerInput(OVRInput.Button.One, OVRInput.Button.Two, OVRInput.Button.SecondaryIndexTrigger, OVRInput.Button.SecondaryHandTrigger);
        leftHand = new ControllerInput(OVRInput.Button.Three, OVRInput.Button.Four, OVRInput.Button.PrimaryIndexTrigger, OVRInput.Button.PrimaryHandTrigger);
    }

    private void Awake()
    {
        if (instance != null)
            Destroy(this);
        else
            instance = this;


    }
}
