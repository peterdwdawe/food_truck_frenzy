﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabbableObject : MonoBehaviour
{
    bool rightHighlight = false;
    bool leftHighlight = false;
    Color OriginalAlbedo;

    public bool RightHighlight
    {
        get
        {
            return rightHighlight;
        }
        protected set
        {
            rightHighlight = value;
        }
    }

    public bool LeftHighlight
    {
        get
        {
            return leftHighlight;
        }
        protected set
        {
            leftHighlight = value;
        }
    }
    public bool inHand = false;

    protected void HandleHighlight(bool left, bool right) {
        GetComponent<Renderer>().material.color =  
            right?
            (left? 
                Settings.instance.bothHighlight :   Settings.instance.rightHighlight):
            (left? 
                Settings.instance.leftHighlight :   OriginalAlbedo);
    }

    // Use this for initialization
    protected virtual void Start()
    {
        //GetComponent<Renderer>().material.shader = Shader.Find("graphs/GlowSelect");

        if (GetComponent<Renderer>() != null)
            OriginalAlbedo = GetComponent<Renderer>().material.color;
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (GetComponent<Renderer>() != null)
            HandleHighlight(LeftHighlight, RightHighlight);

    }

    public virtual void Grab(Hand hand)
    {
        resetHighlight();
        hand.currentHeldObject = this;
        inHand = true;
    }

    public virtual void LetGo(Hand hand)
    {
        hand.currentHeldObject = null;
        inHand = false;
    }

    public void resetHighlight()
    {
        RightHighlight = false;
        LeftHighlight = false;
    }

    public virtual void Highlight(Hand hand)
    {
        
        //GetComponent<Renderer>().material.color = hand.highlightColor;
        switch (hand.hand)
        {
            case handSide.right:
                RightHighlight = true;
                break;
            case handSide.left:
                LeftHighlight = true;
                break;
        }
    }

    public virtual void UnHighlight(Hand hand) {
        //GetComponent<Renderer>().material.color = Color.white;
        switch (hand.hand)
        {
            case handSide.right:
                RightHighlight = false;
                break;
            case handSide.left:
                LeftHighlight = false;
                break;
        }
    }
}
