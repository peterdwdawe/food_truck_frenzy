﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Ingredient))]
[CanEditMultipleObjects]
public class IngredientEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Ingredient myScript = (Ingredient)target;
        //This button is used to test where the next ingredient will be added.
        if (GUILayout.Button("Debug Ingredient"))
        {
            myScript.ToggleDebug();
        }
    }
}