﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recepticle : GrabbableObject {

    public GameObject ObjectToSpawn;

    // Use this for initialization
    protected override void Start () {
        base.Start();
	}

    // Update is called once per frame
    protected override void Update () {
        base.Update();
    }

    public override void Grab(Hand hand)
    {
        
        GameObject g = Instantiate(ObjectToSpawn,Vector3.zero,Quaternion.identity,null);
        if(g.GetComponent<GrabbableObject>()!=null)
            g.GetComponent<GrabbableObject>().Grab(hand);
        else
        {
            Destroy(g);
            Debug.Log(ObjectToSpawn.name + " prefab has no grabbable object script attached!");
        }
    }
}
