﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sandwich : PickupObject {
    public Vector3 startAddPosition = Vector3.zero;
    public float addIngredientRadius = 0.1f;
    Vector3 addPosition = Vector3.zero;
    public List<IngredientType> ingredients = new List<IngredientType>();
    
    // Use this for initialization
    protected override void Start () {
        base.Start();
        ingredients.Add(IngredientType.BottomBun);
        addPosition = startAddPosition;
    }

    // Update is called once per frame
    protected override void Update () {
        base.Update();
        Collider[] collidersInRange = Physics.OverlapSphere(transform.TransformPoint(addPosition), addIngredientRadius);

        foreach (Collider c in collidersInRange)
        {
            Ingredient i = c.gameObject.GetComponentInParent<Ingredient>();
            if (i != null)
            {
                if(!(i.inHand || i.inSandwich))
                    addIngredient(i);

            }
        }
    }

    public void addIngredient(Ingredient ingredient)
    {
        ingredients.Add(ingredient.ingredientType); //Add the ingredientType to the list
        ingredient.inSandwich = true;
        ingredient.parentSandwich = this;
        ingredient.transform.parent = transform;
        ingredient.rb.isKinematic = true;
        ingredient.transform.localEulerAngles = ingredient.placementEulerangles;
        ingredient.transform.localPosition = addPosition-transform.InverseTransformVector(ingredient.transform.TransformVector(ingredient.bottomPosition)); //maybe wrong
        rb.mass += ingredient.rb.mass;  // Maybe not necessary
        Destroy(ingredient.rb);
        addPosition = transform.InverseTransformPoint(ingredient.transform.TransformPoint(ingredient.topPosition)); //Weird math that night be wrong but works for now
    }
    //public override void UnHighlight(Hand hand)
    //{
    //    base.UnHighlight(hand);
    //    GrabbableObject[] grabbableChildren = GetComponentsInChildren<GrabbableObject>();
    //    foreach (GrabbableObject g in grabbableChildren)
    //    {
    //        if(!(g is Sandwich))
    //            g.UnHighlight(hand);
    //    }
    //}
}
